package com.example.prueba.peliculas_master.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.prueba.peliculas_master.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.example.prueba.peliculas_master.Modelo.Peliculas;

/**
 * Created by prueba on 13/09/2016.
 */
public class ListPeliculasAdapter extends BaseAdapter {
    List<Peliculas> data;
    Context context;

    public ListPeliculasAdapter(List<Peliculas> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        if (v == null) {
            v = View.inflate(context, R.layout.template_list_peliculas, null);
        }

        ImageView img = (ImageView) v.findViewById(R.id.imagen);
        TextView nombre = (TextView) v.findViewById(R.id.nombre);
        Log.i("Nombre", nombre.getText().toString());
        TextView sinopsis = (TextView) v.findViewById(R.id.sinopsis);
        TextView fechaestreno = (TextView) v.findViewById(R.id.fechaestreno);
        nombre.setText(data.get(i).getName());
        sinopsis.setText(data.get(i).getSinopsis());
        fechaestreno.setText(data.get(i).getFechaestreno());
        Picasso.with(context).load(data.get(i).getImgUrl()).into(img);
        return v;
    }

}
package com.example.prueba.peliculas_master;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.prueba.peliculas_master.Adapter.ListPeliculasAdapter;
import com.example.prueba.peliculas_master.Modelo.Peliculas;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView list;
    List<Peliculas> data;
    ListPeliculasAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = (ListView) findViewById(R.id.list);
        data = new ArrayList<>();

        Peliculas peliculas = new Peliculas();
        peliculas.setName("No Respires");
        peliculas.setSinopsis("Esta pelicula es bastante buena");
        peliculas.setFechaestreno ("12-09-2016");
        peliculas.setImgUrl("http://lasoft.xyz/wp-content/uploads/2016/07/422464.jpg");
        data.add(peliculas);

        Peliculas peliculas1 = new Peliculas();
        peliculas1.setName("El conjuro2");
        peliculas1.setSinopsis("Esta pelicula es bastante miedosa");
        peliculas1.setFechaestreno ("01-01-2016");
        peliculas1.setImgUrl("http://entretenimiento.starmedia.com/imagenes/2016/06/conjuro1.jpg");
        data.add(peliculas1);

        Peliculas peliculas2 = new Peliculas();
        peliculas2.setName("Alicia en el país de las maravillas");
        peliculas2.setSinopsis("Esta pelicula es muy tierna");
        peliculas2.setFechaestreno ("10-02-2014");
        peliculas2.setImgUrl("http://www.dentrocine.com/wp-content/uploads/2010/04/Alicia-en-el-pais-de-las-maravillas.jpg");
        data.add(peliculas2);

        Peliculas peliculas3 = new Peliculas();
        peliculas3.setName("El paseo2");
        peliculas3.setSinopsis("Esta pelicula es chistosa");
        peliculas3.setFechaestreno ("01-03-2015");
        peliculas3.setImgUrl("http://2.bp.blogspot.com/-27Ib9ntpHkg/UfKejbLCyiI/AAAAAAAACcA/zNckT1Eg-1g/s1600/el-paseo-dos.jpg");
        data.add(peliculas3);

        adapter = new ListPeliculasAdapter(data,this);
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (i%2 == 0){
            Toast.makeText(MainActivity.this, "El item seleccionado es par.  "+ "nombre: " +data.get(i).getName() , Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(MainActivity.this, "El item seleccionado es impar.  "+ "nombre: " +data.get(i).getName() , Toast.LENGTH_LONG).show();
        }

    }
}
